#############################
# Autoscaling Group
#############################
# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We implement a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  eks-nodes-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
sudo /etc/eks/bootstrap.sh \
--apiserver-endpoint '${var.eks_endpoint}' \
--b64-cluster-ca '${var.eks_ca}' \
'${var.eks_cluster_name}'
USERDATA
}

resource "aws_launch_template" "eks_nodes_launch_template" {
  name                      = "${var.project}-${var.env}-eks-${var.eks_nodes_tier}-launch-template"
  image_id                  = var.eks_nodes_ami_id
  key_name                  = var.keypair
  instance_type             = var.eks_nodes_type
  network_interfaces {
    security_groups             = var.eks_nodes_sg_ids
    associate_public_ip_address = var.associate_public_ip_address
  }
  user_data                 = base64encode(local.eks-nodes-userdata)

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_type = var.eks_nodes_volume_type
      volume_size = var.eks_nodes_volume_size
      delete_on_termination = true
    }
  }

  tag_specifications {
    resource_type = "instance"

    tags = tomap({
      Name          = "${var.project}-${var.env}-eks-${var.eks_nodes_tier}",
      project       = var.project,
      environment   = var.env,
      terraform     = true,
      component     = "eks",
      role          = "node",
      tier          = var.eks_nodes_tier,
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "owned"
    })
  }
}

resource "aws_eks_node_group" "eks_node" {
  cluster_name    = var.eks_cluster_name
  node_group_name = var.eks_nodes_tier
  node_role_arn   = var.eks_nodes_role_arn
  subnet_ids      = var.main_subnet_app_zone

  launch_template {
    id      = aws_launch_template.eks_nodes_launch_template.id
    version = "$Latest"
  }

  labels = var.eks_nodes_labels

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  scaling_config {
    desired_size = var.eks_nodes_group.asg_desired_capacity
    max_size     = var.eks_nodes_group.asg_max_size
    min_size     = var.eks_nodes_group.asg_min_size
  }

  update_config {
    max_unavailable_percentage = 50
  }

  tags = tomap({
    Name          = "${var.project}-${var.env}-eks-${var.eks_nodes_tier}",
    project       = var.project,
    environment   = var.env,
    terraform     = true,
    component     = "eks",
    role          = "node",
    tier          = var.eks_nodes_tier,
    "kubernetes.io/cluster/${var.eks_cluster_name}" = "owned"
    "k8s.io/cluster-autoscaler/${var.eks_nodes_autoscaling}" = "TRUE"
    "k8s.io/cluster-autoscaler/${var.eks_cluster_name}" = "owned"
  })
}

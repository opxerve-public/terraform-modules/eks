variable "project" {
    description = "Project name for this resource"
    type        = string
}
variable "env" {
    description = "Environment name for this resource"
    type        = string
}
variable "eks_version" {
    description = "Version of EKS cluster"
    type        = string
    default     = "1.22"
}
variable "eks_cluster_name" {
    description = "Name of EKS cluster"
    type        = string
}
variable "eks_master_log_retention" {
    description = "Number of EKS master log retention day"
    type        = number
    default     = 30
}
variable "eks_endpoint_private" {
    description = "Open EKS private endpoint"
    type        = bool
    default     = true
}
variable "eks_endpoint_public" {
    description = "Open EKS public endpoint"
    type        = bool
    default     = false
}
variable "eks_node_log_retention" {
    description = "Number of EKS pod log retention day"
    type        = number
    default     = 30
}
variable "aws_ebs_csi_driver_enabled" {
    description = "Enable aws ebs csi driver"
    type        = bool
    default     = true
}
variable "aws_kube_proxy_enabled" {
    description = "Enable kube proxy"
    type        = bool
    default     = true
}
variable "enabled_cluster_log_types" {
    description = "Enable aws ebs csi driver"
    type        = list(string)
    default     = ["audit"]
    # default     = ["api",
    #                "audit",
    #                "authenticator",
    #                "controllerManager",
    #                "scheduler"]
}

#############################
# Main VPC
#############################
variable "main_vpc_id" {
    description = "VPC ID for running EKS cluster"
    type        = string
}
variable "main_subnet_app_1a_id" {
    description = "ID for private subnet a"
    type        = string
}
variable "main_subnet_app_1b_id" {
    description = "ID for private subnet b"
    type        = string
}
variable "main_subnet_app_1c_id" {
    description = "ID for private subnet c"
    type        = string
}

#############################
# MGMT VPC
#############################
variable "mgmt_vpc_cidr_block" {
    description = "Management VPC cidr block"
    type        = string
}

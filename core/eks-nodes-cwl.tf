#############################
# Cloud Watch Policy
#############################
resource "aws_cloudwatch_log_group" "eks_nodes_loggroup" {
  name              = "${var.project}-${var.env}-eks-nodes-log"
  retention_in_days = var.eks_node_log_retention

  tags = merge(
    local.default_tags,
    {
      Name = "${var.project}-${var.env}-eks-nodes-log"
    } 
  )
}

resource "aws_iam_policy" "eks_nodes_log_policy" {
  name       = "${var.project}-${var.env}-eks-nodes-log-policy"
  path       = "/"
  policy     = data.aws_iam_policy_document.eks_nodes_log_policy.json
}

resource "aws_iam_role_policy_attachment" "eks_nodes_role_logPolicy" {
  policy_arn = aws_iam_policy.eks_nodes_log_policy.arn
  role       = aws_iam_role.eks_nodes_role.name
}

resource "aws_iam_role_policy_attachment" "eks_nodes_cloudwatch_readonly_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess"
  role       = aws_iam_role.eks_nodes_role.name
}

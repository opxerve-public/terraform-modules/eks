resource "aws_eks_addon" "cni" {
  cluster_name = var.eks_cluster_name
  addon_name   = "vpc-cni"
}

resource "aws_eks_addon" "core-dns" {
  cluster_name = var.eks_cluster_name
  addon_name   = "coredns"
}

resource "aws_eks_addon" "kube-proxy" {
  count        = var.aws_kube_proxy_enabled ? 1 : 0
  cluster_name = var.eks_cluster_name
  addon_name   = "kube-proxy"
}

resource "aws_eks_addon" "aws-ebs-csi-driver" {
  count        = var.aws_ebs_csi_driver_enabled ? 1 : 0
  cluster_name = var.eks_cluster_name
  addon_name   = "aws-ebs-csi-driver"
  service_account_role_arn = aws_iam_role.eks_aws_ebs_csi_role.arn
}

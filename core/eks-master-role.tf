#############################
# IAM Role
#############################
resource "aws_iam_role" "eks_master_role" {
  name               = "${var.project}-${var.env}-eks-master-role"
  description        = "${var.project}-${var.env}-eks-master-role"
  assume_role_policy = data.aws_iam_policy_document.eks_master_role_policy.json

  tags = merge(
    local.default_tags,
    {
      Name = "${var.project}-${var.env}-eks-master-role"
    } 
  )
}

resource "aws_iam_role_policy_attachment" "eks_master_role_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_master_role.name
}

resource "aws_iam_role_policy_attachment" "eks_master_role_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_master_role.name
}

resource "aws_iam_openid_connect_provider" "eks_master" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.eks_cert.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.eks_master_cluster.identity[0].oidc[0].issuer
}

#############################
# AWS Loadbalancer Controller
#############################
resource "aws_iam_policy" "aws_lb_ctl_policy" {
  name       = "${var.project}-${var.env}-aws-lb-ctl-policy"
  path       = "/"
  policy     = file("${path.module}/aws-lb-ctl-policies.json")
}

resource "aws_iam_role" "eks_aws_lb_ctl_role" {
  name               = "${var.project}-${var.env}-aws-lb-ctl-role"
  assume_role_policy  = data.aws_iam_policy_document.eks_assume_aws_lb_ctl_role_policy.json
}

resource "aws_iam_role_policy_attachment" "aws_lb_ctl_policy" {
  policy_arn = aws_iam_policy.aws_lb_ctl_policy.arn
  role       = aws_iam_role.eks_aws_lb_ctl_role.name
}

#############################
# AWS EBS CSI driver
#############################
resource "aws_iam_role" "eks_aws_ebs_csi_role" {
  name                = "${var.project}-${var.env}-aws-ebs-csi-role"
  assume_role_policy  = data.aws_iam_policy_document.eks_assume_aws_ebs_csi_role_policy.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"]
}

#############################
# AWS Cluster auto scaler
#############################
resource "aws_iam_policy" "eks_cluster_auto_scaler_policy" {
  name       = "${var.project}-${var.env}-aws-cluster-auto-scaler-policy"
  path       = "/"
  policy     = templatefile("${path.module}/aws-cluster-auto-scaler-policies.tftpl", {
    cluster_name = var.eks_cluster_name
  })
}

resource "aws_iam_role" "eks_cluster_auto_scaler_role" {
  name               = "${var.project}-${var.env}-aws-cluster-auto-scaler-role"
  assume_role_policy  = data.aws_iam_policy_document.eks_assume_aws_cluster_auto_scaler_role_policy.json
}

resource "aws_iam_role_policy_attachment" "eks_cluster_auto_scaler_policy" {
  policy_arn = aws_iam_policy.eks_cluster_auto_scaler_policy.arn
  role       = aws_iam_role.eks_cluster_auto_scaler_role.name
}
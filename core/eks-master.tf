#############################
# EKS Cluster
#############################
resource "aws_eks_cluster" "eks_master_cluster" {
  name        = var.eks_cluster_name
  role_arn    = aws_iam_role.eks_master_role.arn
  version     = var.eks_version
  enabled_cluster_log_types = var.enabled_cluster_log_types

  vpc_config {
    security_group_ids      = [aws_security_group.eks_master_sg.id]
    subnet_ids              = [var.main_subnet_app_1a_id,
                               var.main_subnet_app_1b_id,
                               var.main_subnet_app_1c_id]
    endpoint_private_access = true
    endpoint_public_access  = false
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks_master_role_AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks_master_role_AmazonEKSServicePolicy,
    aws_cloudwatch_log_group.eks_master_loggroup
  ]

  tags = merge(
    local.default_tags,
    {
      Name = "${var.project}-${var.env}-eks-master"
    } 
  )
}

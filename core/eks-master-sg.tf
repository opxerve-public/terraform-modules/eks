#############################
# Security Group
#############################
resource "aws_security_group" "eks_master_sg" {
  name        = "${var.project}-${var.env}-eks-master-sg"
  description = "${var.project}-${var.env}-eks-master-sg"
  vpc_id      = var.main_vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.default_tags,
    {
      Name = "${var.project}-${var.env}-eks-master-sg"
    } 
  )
}

resource "aws_security_group_rule" "eks_master_nodes_https" {
  description              = "Allow nodes"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_master_sg.id
  source_security_group_id = aws_security_group.eks_nodes_sg.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks_master_vpn_https" {
  description              = "Allow vpn vpc"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_master_sg.id
  cidr_blocks              = [var.mgmt_vpc_cidr_block]
  type                     = "ingress"
}
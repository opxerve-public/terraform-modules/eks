## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| eks\_cluster\_name | Name of EKS cluster | `string` | n/a | yes |
| env | Environment name for this resource | `string` | n/a | yes |
| main\_subnet\_app\_1a\_id | ID for private subnet a | `string` | n/a | yes |
| main\_subnet\_app\_1b\_id | ID for private subnet b | `string` | n/a | yes |
| main\_subnet\_app\_1c\_id | ID for private subnet c | `string` | n/a | yes |
| main\_vpc\_id | VPC ID for running EKS cluster | `string` | n/a | yes |
| mgmt\_vpc\_cidr\_block | Management VPC cidr block | `string` | n/a | yes |
| project | Project name for this resource | `string` | n/a | yes |
| eks\_endpoint\_private | Open EKS private endpoint | `bool` | `true` | no |
| eks\_endpoint\_public | Open EKS public endpoint | `bool` | `false` | no |
| eks\_master\_log\_retention | Number of EKS master log retention day | `number` | `30` | no |
| eks\_node\_log\_retention | Number of EKS pod log retention day | `number` | `30` | no |
| eks\_version | Version of EKS cluster | `string` | `"1.17"` | no |

## Outputs

| Name | Description |
|------|-------------|
| config-map-aws-auth | aws-auth.yaml for create access permission |
| eks\_ca | EKS cluster ca |
| eks\_cluster\_name | Name of EKS cluster |
| eks\_endpoint | EKS cluster api endpoint |
| eks\_nodes\_instance\_profile\_name | Kube config for this cluster |
| eks\_nodes\_sg\_id | ID of EKS node security group |
| kubeconfig | Kube config for this cluster |


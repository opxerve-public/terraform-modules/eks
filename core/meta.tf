#############################
# EKS master policies
#############################
data "aws_iam_policy_document" "eks_master_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "eks_master_log_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams"
    ]
    resources = [
      aws_cloudwatch_log_group.eks_master_loggroup.arn
    ]
  }
}

data "tls_certificate" "eks_cert" {
  url = aws_eks_cluster.eks_master_cluster.identity[0].oidc[0].issuer
}

data "aws_iam_policy_document" "eks_assume_aws_lb_ctl_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = local.eks_oidc_url
      values   = ["system:serviceaccount:kube-system:aws-load-balancer-controller"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.eks_master.arn]
      type        = "Federated"
    }
  }
}

data "aws_iam_policy_document" "eks_assume_aws_ebs_csi_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = local.eks_oidc_url
      values   = ["system:serviceaccount:kube-system:ebs-csi-controller-sa"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.eks_master.arn]
      type        = "Federated"
    }
  }
}

data "aws_iam_policy_document" "eks_assume_aws_cluster_auto_scaler_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = local.eks_oidc_url
      values   = ["system:serviceaccount:kube-system:cluster-autoscaler"]
    }

    principals {
      identifiers = [aws_iam_openid_connect_provider.eks_master.arn]
      type        = "Federated"
    }
  }
}

#############################
# EKS nodes policies
#############################
data "aws_iam_policy_document" "eks_nodes_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "eks_nodes_log_policy" {
  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams"
    ]
    resources = [
      aws_cloudwatch_log_group.eks_nodes_loggroup.arn
    ]
  }
}

data "aws_iam_policy_document" "eks_nodes_ses_policy" {
  statement {
    effect = "Allow"
    actions = [
      "ses:SendRawEmail",
      "ses:SendEmail"
    ]
    resources = [
      "*"
    ]
  }
}
### Outputs ###

locals {
  config-map-aws-auth = <<CONFIGMAPAWSAUTH
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.eks_nodes_role.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH

  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${aws_eks_cluster.eks_master_cluster.certificate_authority.0.data}
    server: ${aws_eks_cluster.eks_master_cluster.endpoint}
  name: ${var.eks_cluster_name}
contexts:
- context:
    cluster: ${var.eks_cluster_name}
    user: ${var.eks_cluster_name}
  name: ${var.eks_cluster_name}
current-context: ${var.eks_cluster_name}
kind: Config
preferences: {}
users:
- name: ${var.eks_cluster_name}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1
      command: aws
      args:
        - "eks"
        - "get-token"
        - "--cluster-name"
        - "${var.eks_cluster_name}"
KUBECONFIG

  aws-lb-ctrl-sa = <<MANIFEST
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: aws-load-balancer-controller
  name: aws-load-balancer-controller
  namespace: kube-system
  annotations:
    eks.amazonaws.com/role-arn: ${aws_iam_role.eks_aws_lb_ctl_role.arn}
MANIFEST
}

output "config-map-aws-auth" {
  description = "aws-auth.yaml for create access permission"
  value = local.config-map-aws-auth
}

output "kubeconfig" {
  description = "Kube config for this cluster"
  value = local.kubeconfig
}

output "aws-lb-ctrl-sa" {
  description = "Aws lb ctrl sa for eks"
  value = local.aws-lb-ctrl-sa
}

output "eks_nodes_role_arn" {
  description = "IAM role ARN for eks node"
  value = aws_iam_role.eks_nodes_role.arn
}

output "eks_endpoint" {
  description = "EKS cluster api endpoint"
  value = aws_eks_cluster.eks_master_cluster.endpoint
}

output "eks_ca" {
  description = "EKS cluster ca"
  value = aws_eks_cluster.eks_master_cluster.certificate_authority.0.data
}

output "eks_cluster_name" {
  description = "Name of EKS cluster"
  value = var.eks_cluster_name
}

output "eks_master_sg_id" {
  description = "ID of EKS node security group"
  value = aws_security_group.eks_master_sg.id
}

output "eks_nodes_sg_id" {
  description = "ID of EKS node security group"
  value = aws_security_group.eks_nodes_sg.id
}

output "eks_oidc_provider_url" {
  value = aws_eks_cluster.eks_master_cluster.identity[0].oidc[0].issuer
}

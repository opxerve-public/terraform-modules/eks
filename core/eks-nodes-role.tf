#############################
# IAM Role
#############################
resource "aws_iam_role" "eks_nodes_role" {
  name               = "${var.project}-${var.env}-eks-nodes-role"
  description        = "${var.project}-${var.env}-eks-nodes-role"
  assume_role_policy = data.aws_iam_policy_document.eks_nodes_role_policy.json

  tags = merge(
    local.default_tags,
    {
      Name  = "${var.project}-${var.env}-eks-nodes-role"
    } 
  )
}

resource "aws_iam_role_policy_attachment" "eks_nodes_role_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes_role.name
}

resource "aws_iam_role_policy_attachment" "eks_nodes_role_AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes_role.name
}

resource "aws_iam_role_policy_attachment" "eks_nodes_role_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes_role.name
}

resource "aws_iam_policy" "eks_nodes_ses_policy" {
  name       = "${var.project}-${var.env}-eks-nodes-ses-policy"
  path       = "/"
  policy     = data.aws_iam_policy_document.eks_nodes_ses_policy.json
}

resource "aws_iam_role_policy_attachment" "eks_nodes_role_sesPolicy" {
  policy_arn = aws_iam_policy.eks_nodes_ses_policy.arn
  role       = aws_iam_role.eks_nodes_role.name
}

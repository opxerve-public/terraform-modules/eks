locals{
  default_tags = {
    project     = var.project
    environment = var.env
    terraform   = true
    component   = "eks"
  }

  eks_oidc_url = "${replace(aws_iam_openid_connect_provider.eks_master.url, "https://", "")}:sub"
}

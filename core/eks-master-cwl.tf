#############################
# Cloud Watch Policy
#############################
resource "aws_iam_policy" "master_log_policy" {
  name   = "${var.project}-${var.env}-eks-master-log-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.eks_master_log_policy.json
}

resource "aws_cloudwatch_log_group" "eks_master_loggroup" {
  name              = "/aws/eks/${var.eks_cluster_name}/cluster"
  retention_in_days = var.eks_master_log_retention

  tags = merge(
    local.default_tags,
    {
      Name = "${var.project}-${var.env}-eks-master-log"
    } 
  )
}

resource "aws_iam_role_policy_attachment" "eks_master_log_policy" {
  policy_arn = aws_iam_policy.master_log_policy.arn
  role       = aws_iam_role.eks_master_role.name
}

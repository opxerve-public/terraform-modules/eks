#############################
# Security Group
#############################
resource "aws_security_group" "eks_nodes_sg" {
  name        = "${var.project}-${var.env}-eks-nodes-sg"
  description = "${var.project}-${var.env}-eks-nodes-sg"
  vpc_id      = var.main_vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    local.default_tags,
    {
      Name  = "${var.project}-${var.env}-eks-nodes-sg",
      "kubernetes.io/cluster/${var.eks_cluster_name}" = "owned",
    } 
  )
}

resource "aws_security_group_rule" "eks_master_to_nodes" {
  description              = "Allow master"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  security_group_id        = aws_security_group.eks_nodes_sg.id
  source_security_group_id = aws_security_group.eks_master_sg.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks_mgmt_ssh_to_nodes" {
  description              = "Allow VPN vpc to SSH"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = aws_security_group.eks_nodes_sg.id
  cidr_blocks              = [var.mgmt_vpc_cidr_block]
  type                     = "ingress"
}

resource "aws_security_group_rule" "eks_nodes_self" {
  description              = "Allow nodes itself"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  security_group_id        = aws_security_group.eks_nodes_sg.id
  self                     = true
  type                     = "ingress"
}
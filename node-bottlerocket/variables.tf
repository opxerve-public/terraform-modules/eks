variable "project" {
    description = "Project name for this resource"
    type        = string
}
variable "env" {
    description = "Environment name for this resource"
    type        = string
}
variable "keypair" {
    description = "Keypair for EKS nodes"
    type        = string
}
variable "eks_nodes_ami_id" {
    description = "AMI for EKS nodes"
    type        = string
}
variable "eks_nodes_type" {
    description = "Type of EKS node"
    type        = string
}
variable "eks_nodes_role_arn" {
    description = "IAM role arn for EKS nodes"
    type        = string
}
variable "eks_cluster_name" {
    description = "Name of EKS cluster"
    type        = string
}
variable "eks_endpoint" {
    description = "EKS cluster api endpoint"
    type        = string
}
variable "eks_ca" {
    description = "EKS cluster certificate authority"
    type        = string
}
variable "eks_nodes_sg_ids" {
    description = "List of EKS node security group ID"
    type        = list(string)
}
variable "eks_nodes_labels" {
    description = "Additional labels for EKS nodes"
    type        = map
}
variable "eks_nodes_tier" {
    description = "Node tier name"
    type        = string
}
variable "eks_nodes_volume_size" {
    description = "EBS size for EKS nodes"
    type        = number
    default     = 150
}
variable "eks_nodes_volume_type" {
    description = "EBS type for EKS nodes"
    type        = string
    default     = "gp2"
}
variable "eks_nodes_group" {
    description = "A list of maps defining node group configurations to be defined using AWS Launch Templates."
    type = object({
        asg_desired_capacity    = number,
        asg_max_size            = number,
        asg_min_size            = number,
    })
}
variable "eks_nodes_autoscaling" {
    description = "enabled or disabled"
    type        = string
    default     = "disabled"
}
variable "schedule_power_on_off_enabled" {
    description = "Enable schedule time for automatically power on/off"
    type        = bool
    default     = false
}
variable "associate_public_ip_address" {
    description = "Associate public ip to EC2"
    type        = bool
    default     = null
}
#############################
# Main VPC
#############################

variable "main_subnet_app_zone" {
  description = "main subnet app zone"
  type        = list(string)
}
